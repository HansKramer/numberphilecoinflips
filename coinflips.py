#! /usr/bin/python

import random

#gen = random
#gen = random.SystemRandom()
gen = random.WichmannHill()

def get_streak(n):
    global gen
    return [ gen.randint(0,1) for _ in range(n) ]

def max_streak(streak):
    max = 0
    n = 1
    current = streak[0]
    for r in streak[1:]:
        if r != current:
            if n > max:
                max = n
            n = 1
            current = r
        else:
            n += 1
    if n > max:
        max = n
    return max


distr = [0]*20
samples=100000
for _ in range(samples):
    s = get_streak(20)
    n = max_streak(s)
    distr[n-1] += 1
c_distr = [100.0*x/samples for x in distr]
print c_distr
print sum(c_distr[0:3])


